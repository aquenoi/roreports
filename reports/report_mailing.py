
# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta
from reports.models import ReportType, RecipientGroup, Recipient
from django.db.models import Q
import os

import re
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

#should be raised in case of error during reporting and mailing reports. 
#TODO: mailing with smtp module.
def autoreport_error(report=None, error = None):
	fromadrs = 'error_reports@jgh.mcgill.ca'
	toadrs = 'aquenoi@jgh.mcgill.ca'
	sendmail_location = "/usr/sbin/sendmail"
	if report:
		rep_name = report.name
	else:
		rep_name = 'None' 
	mail_body = u"""From: %s\nTo: %s\nSubject: Error on automatic report : %s\nContent-Type:text/html\n\nAn error has been raised for the report %s: %s
				"""%(fromadrs,toadrs,rep_name, rep_name, error)
	try:
		p = os.popen("%s -tv" % sendmail_location, "w")
		p.write(mail_body)
		status = p.close()
	except IOError, err:
		logfile = open("//webapps//django_app//roreports//reports//rep_auto//mailing.log",'a')
		logfile.write("Error while sending error mail(s): %s"%err)
	
	try:
		logfile = open("//webapps//django_app//roreports//reports//rep_auto//mailing.log",'a')
	except IOError, err:
		pass
	now = datetime.now()
	logfile.write("%s : Error on report %s : %s\n"%now, rep_name,error)
	logfile.close()


#function taking as parameter a single report, executing it and mailing the upcoming results to the list of users setup in admin interface
#also returns a test page.
#parameters: report : the report object; qres: the results you would like to mail, in case of specific query already submitted, qres should already be the results array.
def mail_report(report,qres = None, start_date = None, end_date = None):
	fromadrs = 'noreply_roreports@jgh.mcgill.ca'
	toadrs = []
	rep_name = report.name
	if qres == None:
		startdate = date.today()+timedelta(days = report.delta_from_date)
		enddate = date.today()+timedelta(days = report.delta_to_date)
		query_res = report.submit_query(startdate,enddate)
	else:
		query_res = qres
		startdate = start_date
		enddate = end_date
	
	#case of a query returning 0 rows, except for the headers. Just filling logs.
	if len(query_res) == 1 :
		logfile = open("//webapps//django_app//roreports//reports//rep_auto//mailing.log",'a')
		now = datetime.now()
		logfile.write("""%s : %s empty.\n"""%(now, rep_name))
		logfile.close()
		return None
	#case if the report has no recipient setup in the admin panel.
	if len(report.recipients.all()) == 0 :
		logfile = open("//webapps//django_app//roreports//reports//rep_auto//mailing.log",'a')
		now = datetime.now()
		logfile.write("""%s : %s with no recipient setup.\n"""%(now, rep_name))
		logfile.close()
		return None

	for mail in report.recipients.all():
		toadrs.append(mail.email)
		
	body_mail_text = report.custom_mail_text.replace("@titre@",rep_name).replace("@startdate@",str(startdate)).replace("@enddate@",str(enddate))
	mail_body = u"""\
					<html>
					<head></head>
					   <body>
					   	%s<br/>
					   	<table border = "1" style="text-align:center;">
					   	<thead>
					   	<tr>
				"""%(body_mail_text)

	for cell in query_res[0]:
		if cell:
			mail_body+="<th>"+str(cell)+"</th>"
		else:
			mail_body+="<th>None</th>"
	mail_body+="</tr>"

	for line in query_res[1:]:
		mail_body+="<tr>"
		for cell in line:
			if cell:
				mail_body+= "<td>"+str(cell)+"</td>"
			else:
				mail_body+= "<td>None</td>"
	mail_body+="</table></body></html>"

	msg = MIMEMultipart('alternative')
	msg['Subject']= "Rapport : "+rep_name
	msg['From']=fromadrs
	msg['To']= ", ".join(toadrs)
	#msg['Date']=formatdate(localtime=True)
	mail_part = MIMEText(mail_body,_subtype = 'html',_charset="UTF-8")
	msg.attach(mail_part)
	#mail_body = msg.as_string()
	
	smtp = smtplib.SMTP('10.131.203.44')
	smtp.sendmail(fromadrs,toadrs,msg.as_string())
	#smtp.close()
	smtp.quit()
	logfile = open("//webapps//django_app//roreports//reports//rep_auto//mailing.log",'a')
	now = datetime.now()
	logfile.write("""%s : %s sent to %s.\n"""%(now, rep_name,toadrs))
	logfile.close()
	return mail_body

