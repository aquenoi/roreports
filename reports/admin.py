from django.contrib import admin

from reports.models import RecipientGroup, Recipient, ReportCategory, ReportType

class ReportTypeAdmin(admin.ModelAdmin):
	list_display = ('name','category','schedule')
	fieldsets = [
		('General',			{'fields':['name','category','query_text','active','show_on_list']}),
		('Automatic Reports', {'fields':['schedule','recipients','day_of_the_week','day_of_the_month','custom_mail_text','delta_from_date','delta_to_date']})
	]

class RecipientAdmin(admin.ModelAdmin):
	list_display=('name','group','email')

class RecipientGroupAdmin(admin.ModelAdmin):
	list_display=('name')

admin.site.register(RecipientGroup)
admin.site.register(Recipient, RecipientAdmin)
admin.site.register(ReportCategory)
admin.site.register(ReportType, ReportTypeAdmin)


