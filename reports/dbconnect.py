#import Sybase
import pymssql
from datetime import date,timedelta, datetime

class Aria_connect:
	def __init__(self):
		self.db = pymssql.connect(host = r'10.131.204.243:1433', user = 'reports', password = r'reports', database = 'variansystem')
		self.dbc = self.db.cursor()


	def query(self,query_txt):
		self.dbc.execute(query_txt)
		columns = self.dbc.description 
		res_query = self.dbc.fetchall()
		#result = [{columns[index][0]:column for index, column in enumerate(value)} for value in self.dbc.fetchall()]
		columns_clean = []
		result = []
		for elem in columns:
			columns_clean.append(elem[0])
		result.append(columns_clean)
		result.extend(res_query)
		return result

	def __del__(self):
		self.dbc.close()
		self.db.close()

