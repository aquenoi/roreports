from django.conf.urls import patterns, url
#from django.conf.urls.defaults import *
#from django.conf.urls.defaults import *
#from django.views.generic.simple import direct_to_template

from reports import views


urlpatterns = patterns('reports.views', 
	url(r'^$', views.index, name = 'index'),
	url(r'^excel_export/$', views.excel_export, name = 'xls_exp'),
	url(r'^report_auto/(?P<schedule_type>[ANDWMY])/$',views.report_auto, name = 'report_auto'),
	url(r'^parking/$',views.parking_report, name = 'parking_report'),
	#url(r'^res/(?P<report_id>\d+)/$', views.result, name = 'result')
	url(r'^patient_search/$',views.patient_search, name = 'patient_search'),
	url(r'^patient_search/(?P<pid>\d+)/$','patient_search'),
	url(r'^deceased_list/$',views.deceased_list, name = 'deceased_list'),
	url(r'^deceased_list_xls/$',views.excel_deceased_export, name ='excel_deceased_export'),
)
