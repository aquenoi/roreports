# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, Form
from django.contrib.admin.widgets import AdminTextInputWidget
from datetime import date, datetime, timedelta
from reports.models import ReportType, ReportCategory




class QueryForm(Form):
	CHOICES = [('1',"Today"),('2','This week'),('3','Custom dates :') ]
	reportlist_field = forms.ChoiceField(choices = (),required=True)
	date_select = forms.ChoiceField(widget = forms.RadioSelect, choices = CHOICES,required=True, initial='1' )
	startdate = forms.DateField(widget=forms.DateInput(attrs={'id':'id_startdate'}),label = 'From', initial =datetime.today())
	enddate = forms.DateField(widget=forms.DateInput(attrs={'id':'id_enddate'}),label = 'To', initial = date.today() + timedelta(days = 1))

	def __init__(self, *args, **kwargs):
		super(QueryForm,self).__init__(*args,**kwargs)
		reportlist = []
		for cat in ReportCategory.objects.all():
			reportlist.append(
				(
					cat.name,
					[
						(rep.id, rep.name) for rep in ReportType.objects.filter(category = cat).filter(active=True).filter(show_on_list=True).order_by('name')
					]
				)
			)
		self.fields['reportlist_field'].choices = reportlist

	# def clean(self):
	# 	cleaned_data = super(QueryForm,self).clean()
	# 	date_sel = cleaned_data.get('date_select')
	# 	startdate = cleaned_data.get('startdate')
	# 	enddate = cleaned_data.get('enddate')
	# 	if not date_sel:
	# 		raise forms.ValidationError('You need to select a date range')
	# 	elif date_sel == '3' and not startdate or not enddate:
	# 		raise forms.ValidationError('You need to fill both date fields')
	# 	else:
	# 		return cleaned_data

class Patient_Form(forms.Form):
	patient_id = forms.IntegerField(
					min_value = 1,
					label = 'Patient Id'
					)

class UploadFileForm(forms.Form):
	file_deceased = forms.FileField(
		label = 'Select a file',
	)