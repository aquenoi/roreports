# -*- coding: utf8 -*-
#from django.shortcuts import render

from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.views.decorators.csrf import csrf_protect

from datetime import date, datetime, timedelta
from reports.rep_forms import QueryForm, Patient_Form, UploadFileForm
from reports.models import ReportType, ReportCategory
import xlrd
import xlwt
import re
from report_mailing import mail_report

from reports.dbconnect import Aria_connect
#from xlrd import open_workbook
# Create your views here.


@login_required
def index(request):
	list_dates = [date.today(),date.today()]
	if request.method == 'POST':
		form_rep_list = QueryForm(request.POST)
		if form_rep_list.is_valid():
			query_txt = form_rep_list.cleaned_data['reportlist_field']			
			def today():
				list_dates[1] = (datetime.today() + timedelta(days = 1)).date()
			def week():
				list_dates[0] = (datetime.today() - timedelta(days = datetime.today().weekday())).date()
				list_dates[1] = list_dates[0] + timedelta(days = 5)
			def custom():
				list_dates[0] = form_rep_list.cleaned_data['startdate']
				list_dates[1] = form_rep_list.cleaned_data['enddate']
				
			switch = {
				'1': today,
				'2': week,
				'3': custom,
			}
			
			report_obj = ReportType.objects.get(pk=query_txt)
			if not report_obj:
				error = "rapport non trouve"
			else:
				error = ''
				switch[form_rep_list.cleaned_data['date_select']]()
				query_res = report_obj.submit_query(list_dates[0], list_dates[1])
				
			res_report = report_obj.as_dict()
			request.session['report_res'] = query_res
			request.session['report_dates'] = list_dates
			request.session['report_name'] = res_report['name']
			return render(request, 'reports/result.html', {'report': res_report,
											'query':query_txt, 
											'err':error,
											'qres':query_res,
											'sdate': list_dates[0],
											'edate': list_dates[1],
											'rep_name': res_report['name'],
											 })
		else: 
			error = 'You have an unexpected error with the form. Please contact an administrator'
			return render(request, 'reports/result.html', {'err':error})
	else:
		form_rep_list = QueryForm()
		return render(request, 'reports/index.html',{
			'form_rep_list': form_rep_list, 
    	})

@login_required	
def excel_export(request):
	response = HttpResponse(content_type="application/ms-excel")
	if request.session['report_res']:
		result = request.session['report_res']
		filename = request.session['report_name'] + '_from_' + str(request.session['report_dates'][0]) +'_to_' + str(request.session['report_dates'][1]) 
	else:
		return render(request, 'reports/index.html',{'err':"No data returned, can't export to xls"})

	response['Content-Disposition'] = "attachment; filename=%s.xls" % (filename)
	wb = xlwt.Workbook()
	style = xlwt.easyxf('font:bold 1')
	ws = wb.add_sheet('Sheet1',cell_overwrite_ok = True)
	col= 0
	row = 1
	
	
	for col_name in result[0]:
		ws.write(0, col, col_name,style)
		col+=1
	col = 0
	for line in result[1:]:
		for val in line:
			ws.write(row,col,val)
			col+=1
		row+=1
		col = 0
	ws.col(0).width = 256 * 20 
	ws.col(1).width = 256 * 20 
	ws.col(2).width = 256 * 12 
	ws.col(3).width = 256 * 20 
	ws.col(4).width = 256 * 20 
	ws.col(5).width = 256 * 20 
	wb.save(response)
	return response


def report_auto(request, schedule_type):
	list_report = ReportType.objects.filter(schedule = schedule_type).order_by('name')
	assert list_report
	if schedule_type == 'M':
		day_of_month = datetime.today().day
		list_report = list_report.filter(Q(day_of_the_month = day_of_month))
	elif (schedule_type == 'W'):
		day_of_week = str(datetime.today().weekday())
		list_report = list_report.filter(Q(day_of_the_week = day_of_week))
	
	for report in list_report:
		mail_body = mail_report(report)
		
	return render(request, 'reports/test_report_auto.html', {'list_report':list_report,
													  'mail_body':mail_body,
													})

def parking_report(request):
	prep = ReportType.objects.filter(name = 'Parking patient list')[0]
	today = datetime.today()
	startdate = (today - timedelta(days=today.weekday()-5) ).date()
	enddate = startdate + timedelta(days=7)
	spec_enddate = enddate + timedelta(days=28) 
	qres = prep.submit_query(startdate, enddate, startdate, spec_enddate)
	mail_text = mail_report(prep, qres, startdate, enddate)
	return render(request, 'reports/result_parking.html', {'report': prep.name,
											'query':prep.query_text, 
											'qres':qres,
											'mail_text':mail_text
											})

@csrf_protect
def patient_search(request, pid=None):
	# if pid is not None:
	# 	patient_form = Patient_Form()
	# 	query = None
	# 	report = ReportType.objects.filter(name = 'patient search')[0]
	# 	query = report.submit_query(patient_id = pid)
	# #	query = Aria_dashboard().db_patient_search(pid)
	# elif request.method == 'POST':
	# 	patient_form = Patient_Form(request.POST)
	# 	if patient_form.is_valid():
	# 		pid = patient_form.cleaned_data['patient_id']
	# 		query = report.submit_query(patient_id = pid)
	# 		#query = Aria_dashboard().db_patient_search(pid)
	# else:
	# 	patient_form = Patient_Form()
	# 	query = []
	# return render(request,'reports/patient_search.html', {'title': 'Patient search', 'patient_form':patient_form,'query':query})
	report = ReportType.objects.filter(name = 'patient search')[0]
	if pid is not None:
		patient_form = Patient_Form()
		query = None
		query = report.submit_query(start_date = None, patient_id = pid)
	elif request.method == 'POST':
		patient_form = Patient_Form(request.POST)
		if patient_form.is_valid():
			pid = patient_form.cleaned_data['patient_id']
			query = report.submit_query(start_date = None, patient_id = pid)
	else:
		patient_form = Patient_Form()
		query = []
	return render(request,'reports/patient_search.html', {'title': 'Patient search', 'patient_form':patient_form,'query':query})

@csrf_protect
def deceased_list(request):
	nsheet = 'null'
	listid = {}
	query = ''
	patient_list = ''
	final_plist = []
	if request.method == 'POST':
		form = UploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			paramFile = request.FILES['file_deceased'].read()	
			#portfolio = csv.DictReader(paramFile)
			book = xlrd.open_workbook(file_contents=paramFile)
			nsheet = book.sheets()
			page1 = nsheet[0]
			for row in range(page1.nrows):
				pid = re.search(r'\d+',str(page1.cell(row,0).value))
				if pid:
					date_d = page1.cell(row, 3).value
					listid[pid.group(0)] =  str( datetime(*xlrd.xldate_as_tuple(date_d, book.datemode)))
					
			query = """
				select Patient.FirstName,
				Patient.LastName,
				Patient.PatientId,
				Patient.PatientId2,	
				'Oncologist' = ( 
                                	SELECT Doctor.LastName
                                	FROM 
                                        	PatientDoctor,
                                        	Doctor
                                	WHERE
                                        	PatientDoctor.PatientSer = Patient.PatientSer
                                        	AND PatientDoctor.ResourceSer = Doctor.ResourceSer
                                        	AND PatientDoctor.PrimaryFlag = 1
                                        	AND PatientDoctor.OncologistFlag = 1
                                )
				from Patient
				where Patient.PatientId in (
				"""
			for pid in listid.keys():
				query += "'"+pid+"',"
			query= query[:-1]
			query +=')'
			connection = Aria_connect()
        	patient_list = connection.query(query)
        	for patient in patient_list[1:]:
				if listid.has_key(patient[2]):
					tmp = list(patient)
					tmp.append(listid[patient[2]])
					final_plist.append(tmp)
        	request.session['deceased_list'] = final_plist
	else:
		form = UploadFileForm()

	return render(request,'reports/deceased_list.html',{'patient_list':final_plist, 'form': form, 'title': 'List of deceased patients'})


def excel_deceased_export(request):
	response = HttpResponse(content_type="application/ms-excel")
	response['Content-Disposition'] = "attachment; filename=deceased_report_%s.xls" % (str(date.today()))
	wb = xlwt.Workbook()
	ws = wb.add_sheet('Sheet1',cell_overwrite_ok = True)
	col= 0
	row = 1
	result = request.session['deceased_list']
	ws.write(0,0,'Prenom')
	ws.write(0,1,'Nom')
	ws.write(0,2,'Patient Id')
	ws.write(0,3,'No RAMQ')
	ws.write(0,4,'Radio-oncologue')
	ws.write(0,5,'Date')
	for line in result:
		for column in line:
			ws.write(row,col,column)
			col+=1
		row+=1
		col = 0
	ws.col(0).width = 256 * 20 
	ws.col(1).width = 256 * 20 
	ws.col(2).width = 256 * 12 
	ws.col(3).width = 256 * 20 
	ws.col(4).width = 256 * 20 
	ws.col(5).width = 256 * 20 
	wb.save(response)
	return response
