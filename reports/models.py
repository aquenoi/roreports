# -*- coding: utf8 -*-
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.template import Template, Context
from reports.dbconnect import Aria_connect
from datetime import datetime, date, time, timedelta
import iso8601
from reports.custom_fields import DayOfTheWeekField


class RecipientGroup(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name

class Recipient(models.Model):
    name = models.CharField(max_length=200)
    group = models.ForeignKey("RecipientGroup", related_name="recipients")
    email = models.EmailField(max_length=75)
    active = models.BooleanField(default=True)
    def __unicode__(self):
        return self.name 
    def as_dict(self):
        return {
            "name": self.name,
            "group": self.group.name,
        }

class ReportCategory(models.Model):
    class Meta:
        verbose_name_plural='categories'
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class ReportType(models.Model):
    SCHEDULE_CHOICES = (
        ('N','None'),
        ('D','Daily (5/7)'),
        ('A','Daily (7/7)'),
        ('W','Weekly'),
        ('M','Monthly'),
        ('Y','Yearly')
        )


    name = models.CharField(max_length=200, unique=True)
    category = models.ForeignKey("ReportCategory", related_name="reports")
    query_text = models.TextField()
    run_during_clinical_hours = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    show_on_list = models.BooleanField(default=True)
    #run_auto = models.CharField(max_length=200)
    schedule = models.CharField(max_length=1, choices=SCHEDULE_CHOICES,default='N')
    recipients = models.ManyToManyField("Recipient", related_name="reports",blank=True)
    day_of_the_week = DayOfTheWeekField(blank=True)
    day_of_the_month = models.IntegerField(
                            default=1,
                            validators=[
                                MaxValueValidator(31),
                                MinValueValidator(1)
                            ])
    custom_mail_text = models.TextField(blank=True)
    delta_from_date = models.SmallIntegerField(blank=True)
    delta_to_date = models.SmallIntegerField(blank=True)
    def __unicode__(self):
        return self.name


    def as_dict(self):
        return {
            "name": self.name,
            "category": self.category.name,
            "sql_string": self.query_text,
            "active": self.active,
            "show_on_list": self.show_on_list
        }


    def submit_query(self, start_date=None, end_date=None,special_startdate = None, special_enddate = None, patient_id = None):
        query_date = self.query_text
        if start_date != None : 
            if not end_date:
                end_date = start_date + datetime.timedelta(days=1)
            else:
                end_date = end_date + timedelta(days=1)        
            query_date = query_date.replace("@query_from_date@", "'"+str(start_date)+"'")
            query_date = query_date.replace("@query_to_date@", "'"+str(end_date)+"'")

        if patient_id : 
            query_date = query_date.replace("@patient_id@", "'"+str(patient_id)+"'" )

        if special_startdate and special_enddate :
            query_date = query_date.replace("@special_from_date@", "'"+str(special_startdate)+"'")
            query_date = query_date.replace("@special_to_date@", "'"+str(special_enddate)+"'")  
        connection = Aria_connect()
        return connection.query(query_date)
        #return query_date
    def __unicode__(self):
        return self.name
